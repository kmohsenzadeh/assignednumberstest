###############################################################################
###############################################################################
###############################################################################
##
## Copyright ⓒ 2019-2020 Qualcomm Innovation Center, Inc.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
## this list of conditions and the following disclaimer.
##
## 2. Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in the
## documentation and/or other materials provided with the distribution.
##
## 3. Neither the name of the copyright holder nor the names of its
## contributors may be used to endorse or promote products derived from this
## software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################
###############################################################################
###############################################################################

import collections
import os
import pathlib
import pprint
import sys
import yaml

###############################################################################
###############################################################################
###############################################################################

ASSIGNED_NUMBERS = "../assigned_numbers_doc/yaml"

###############################################################################
###############################################################################
###############################################################################

uuids = {}
uuid_types = {}
uuid_names = {}

psms = {}
company_identifiers = {}

errors = 0
warnings = 0

member_services = collections.defaultdict (int)

###############################################################################
###############################################################################
###############################################################################

def parse_uuids (content):
    global errors
    global uuids
    for item in content:
        if 'uuid' in item:
            uuid = int (item['uuid'])
            item_name = item['name']
            item_type = item['type']
            errors += 1
            if uuid in uuids:
                errors += 1
                print (f"ERROR: Duplicate uuid 0x{uuid:04x} {uuids[uuid]['type']} {item['type']}", file = sys.stderr)
                print (f"               {uuids[uuid]['name']} {item['name']}", file = sys.stderr)
                print (f"               {uuids[uuid]['name']} {item['name']}", file = sys.stderr)
                sys.stderr.flush ()
            else:
                if item_name in uuid_names and item['type'] != 'member_service':
                    other = uuid_names[item_name]
                    if item_type != other['type']:
                        print (f"WARNING: Duplicate name \"{item_name}\" : \"{other['name']}\"", file = sys.stderr)
                        print (f"               {item_type} : {other['type']}", file = sys.stderr)
                        sys.stderr.flush ()
                    else:
                        errors += 1
                        print (f"ERROR: Duplicate name \"{item_name}\" : \"{other['name']}\"", file = sys.stderr)
                        print (f"               {item_type} : {other['type']}", file = sys.stderr)
                        sys.stderr.flush ()
                elif item['type'] == 'member_service':
                    member_services[item['name']] += 1
                    item['name'] += f' Service#{member_services[item["name"]]:02d}'
                    uuid_types[item['uuid']] = item['type']
                    uuid_names[item['name']] = item
                else:
                    uuid_names[item['name']] = item
                    uuid_types[item['uuid']] = item['type']
                uuids[uuid] = item

###############################################################################
###############################################################################
###############################################################################

def parse_psms (content):
    global errors
    global psms
    for item in content:
        if 'psm' in item:
            psm = int (item['psm'])
            name = item['name']
            ref = item['reference']
            if psm % 2 == 0:
                errors += 1
                print (f"ERROR: PSM must be odd (0x{psm:04x} {name})")

            psms[psm] = item

###############################################################################
###############################################################################
###############################################################################

def parse_company_identifiers (content):
    global errors
    global company_identifiers
    for item in content:
        if 'value' in item:
            cid = int (item['value'])
            name = item['name']
            if cid in company_identifiers:
                errors += 1
                print (f"ERROR: Company ID duplicated (0x{cid:%04x}")
            else:
                company_identifiers[cid] = item

###############################################################################
###############################################################################
###############################################################################

def validate_uuids ():
    global errors

###############################################################################
###############################################################################
###############################################################################

def validate_psms ():
    global errors

###############################################################################
###############################################################################
###############################################################################

def validate_company_identifiers ():
    global warnings
    names = {}
    for cid, item in company_identifiers.items ():
        name = item['name']
        if name in names:
            warnings += 1
            print (f"WARNING: Duplicate company identifier name {name} 0x{cid:04x} / 0x{names[name]:04x}")
        names[name] = cid

###############################################################################
###############################################################################
###############################################################################

def create_uuid_summary ():
    last_uuid = 0
    for uuid in sorted (uuids):
        item = uuids[uuid]
        if uuid != last_uuid + 1:
            missing = uuid - last_uuid - 1
            print (f"  ... ({missing} missing) ...")
        print (f"0x{uuid:04x} = {item['type']} : {item['name']}")
        last_uuid = uuid

    print ("\n------\n\n")

    for name in sorted (uuid_names):
        item = uuid_names[name]
        print (f"{item['name']} = 0x{uuid:04x} : {item['type']}")
        last_uuid = uuid

    print ("\n------\n\n")
    print (len (uuids), len (uuid_names))

###############################################################################
###############################################################################
###############################################################################

if __name__ == "__main__":
    assigned_number_files = os.listdir (ASSIGNED_NUMBERS)

    for filename in assigned_number_files:
        if filename[-5:] == '.yaml':
            fp = open (pathlib.Path (ASSIGNED_NUMBERS) / filename, "rb")
            data = fp.read ().decode ('utf8')
            fp.close ()
            content = yaml.load (data, Loader = yaml.Loader)
            if content:
                if 'uuids' in content:
                    parse_uuids (content['uuids'])
                if 'psms' in content:
                    parse_psms (content['psms'])
                if 'company_identifiers' in content:
                    parse_company_identifiers (content['company_identifiers'])

    validate_uuids ()
    validate_psms ()
    validate_company_identifiers ()

    if errors > 0:
        if errors > 1:
            plural = 's'
        else:
            plural = ''
        print (f"\nValidation: {errors} Error{plural}")
        exit (1)
    else:
        print ("\nValidation: Success")
        if warnings > 0:
            if warnings > 1:
                plural = 's'
            else:
                plural = ''
            print (f"\n{warnings} Warning{plural}")

###############################################################################
###############################################################################
###############################################################################
