###############################################################################
###############################################################################
###############################################################################
##
## CopyrighTESTING TESTINGt ⓒ 2019-2021 Qualcomm Innovation Center, Inc.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
## this list of conditions and the following disclaimer.
##
## 2. Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in the
## documentation and/or other materials provided with the distribution.
##
## 3. Neither the name of the copyright holder nor the names of its
## contributors may be used to endorse or promote products derived from this
## software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################
###############################################################################
###############################################################################

SOURCES = $(wildcard ../assigned_numbers_doc/yaml/*.yaml)

SCHEMAS = $(wildcard ../assigned_numbers_doc/schema/*.jsonschema)

LATEX = $(wildcard ../templates/spec/*.cls)
LATEX += $(wildcard ../assigned_numbers_doc/src/*.tex)

MEDIAS = $(wildcard ../templates/media/*.eps)
MEDIAS += $(wildcard ../templates/media/*.otf)
MEDIAS += $(wildcard ../templates/media/*.ttf)

TOOLS = $(wildcard *.py)
TOOLS += $(wildcard build/*.py)
TOOLS += $(wildcard validate/*.py)

DATABASE = _build/assigned_numbers.db

ifeq ($(OS),Windows_NT)
	MKDIR = mkdir
	RMDIR = rmdir /s /q
	PYTHON = python.exe
else
	MKDIR = mkdir -p
	RMDIR = rm -rf
	PYTHON = python3
endif

.SUFFIXES:
MAKEFLAGS += --no-builtin-rules

.PHONY: all
all : appearance_values.pdf assigned_numbers.pdf

release : $(DATABASE) $(LATEX) $(MEDIAS) $(TOOLS) build_assigned_numbers.py | _build
	@$(PYTHON) build_assigned_numbers.py ../assigned_numbers_doc/src/release.tex

next_release :
	@$(PYTHON) update_release_number.py ../assigned_numbers_doc/src/release.tex

assigned_numbers.pdf : $(DATABASE) $(LATEX) $(MEDIAS) $(TOOLS) build_assigned_numbers.py | _build
	@echo "Building assigned_numbers.pdf"
	@$(PYTHON) build_assigned_numbers.py

appearance_values.pdf : $(DATABASE) $(LATEX) $(MEDIAS) $(TOOLS) build_appearance_values.py | _build
	@echo "Building appearance_values.pdf"
	@$(PYTHON) build_appearance_values.py

$(DATABASE) : $(SOURCES) $(SCHEMAS) validate_assigned_numbers.py mk_assigned_numbers_database.py | _build
	@echo Validating .yaml files
	@$(PYTHON) validate_assigned_numbers.py
	@$(PYTHON) mk_assigned_numbers_database.py

cross_check : cross_check_output.txt

cross_check_output.txt : $(DATABASE) cross_check.py
	@echo "Cross Checking .yaml against .xlsx"
	@$(PYTHON) cross_check.py > cross_check_output.txt || cat cross_check_output.txt

.PHONY: view
view : assigned_numbers.pdf
	@open assigned_numbers.pdf

_build :
	@$(MKDIR) _build

.PHONY: clean
clean:
	@echo "Cleaning"
	@$(RMDIR) _build

.PHONY: fullclean
fullclean : clean
	@echo "Full Cleaning"
	@$(RMDIR) assigned_numbers.pdf

.PHONY: help
help :
	@echo ""
	@echo "Builds an assigned numbers document"
	@echo ""
	@echo "  make all            builds all"
	@echo "  make release        builds a release build named assigned_numbers_rXX.pdf"
	@echo "  make next_release   increaments the release number (XX = XX + 1)"
	@echo ""
