###############################################################################
###############################################################################
###############################################################################
##
## Copyright ⓒ 2019-2021 Qualcomm Innovation Center, Inc.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
## this list of conditions and the following disclaimer.
##
## 2. Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in the
## documentation and/or other materials provided with the distribution.
##
## 3. Neither the name of the copyright holder nor the names of its
## contributors may be used to endorse or promote products derived from this
## software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################
###############################################################################
###############################################################################

import sqlite3
import yaml
import os
import pathlib

###############################################################################
###############################################################################
###############################################################################

## Sanitize a string
#
#  removing zero width spaces
#  remove spaces at the start and end of the string

def sanitize (s):
    s = s.replace (b'\xe2\x80\x8b'.decode ('utf8'), '') # Zero width space
    return s.strip ()

###############################################################################
###############################################################################
###############################################################################

def parse_uuids (entries, db):
    for entry in entries:
        uuid = int (entry['uuid'])
        name = sanitize (entry['name'])
        if 'id' in entry:
            uri = sanitize (entry['id'])
        else:
            uri = None
        if 'sdo' in entry:
            sdo = sanitize (entry['sdo'])
        else:
            sdo = None
        if 'pending' in entry:
            pending = entry['pending']
        else:
            pending = False
        uuid_type = sanitize (entry['type'])

        # if uuid_type == 'service':
            # name += ' service'
        # if uuid_type == 'characteristic':
            # name += ' characteristic'
        # if uuid_type == 'object_type':
            # name += ' object type'

        db.execute (
            "INSERT INTO uuids (uuid, uri, name, sdo, type, pending) VALUES (?, ?, ?, ?, ?, ?)",
            (
                uuid,
                uri,
                name,
                sdo,
                uuid_type,
                pending
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_company_identifiers (entries, db):
    for entry in entries:
        value = int (entry['value'])
        name = sanitize (entry['name'])

        db.execute (
            "INSERT INTO compid (value, name) VALUES (?, ?)",
            (
                value,
                name
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_hci_version (entries, db):
    for entry in entries:
        value = entry['value']
        name = sanitize (entry['name'])

        db.execute (
            "INSERT INTO hci_version (value, name) VALUES (?, ?)",
            (
                value,
                name
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_lmp_version (entries, db):
    for entry in entries:
        value = entry['value']
        name = sanitize (entry['name'])

        db.execute (
            "INSERT INTO lmp_version (value, name) VALUES (?, ?)",
            (
                value,
                name
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_ll_version (entries, db):
    for entry in entries:
        value = entry['value']
        name = sanitize (entry['name'])

        db.execute (
            "INSERT INTO ll_version (value, name) VALUES (?, ?)",
            (
                value,
                name
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_psms (entries, db):
    for entry in entries:
        psm = entry['psm']
        name = sanitize (entry['name'])
        if 'reference' in entry:
            ref = sanitize (entry['reference'])
        else:
            ref = None
        if 'pending' in entry:
            pending = entry['pending']
        else:
            pending = False

        db.execute (
            "INSERT INTO psms (psm, name, ref, pending) VALUES (?, ?, ?, ?)",
            (
                psm,
                name,
                ref,
                pending
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_ad_types (entries, db):
    for entry in entries:
        tag = entry['tag']
        name = sanitize (entry['name'])
        if 'reference' in entry:
            ref = sanitize (entry['reference'])
        else:
            ref = ''
        if 'pending' in entry:
            pending = entry['pending']
        else:
            pending = False
        if 'notes' in entry:
            notes = entry['notes']
        else:
            notes = None
        if 'nfc_only' in entry:
            nfc_only = entry['nfc_only']
        else:
            nfc_only = False

        db.execute (
            "INSERT INTO ad_types (tag, name, notes, ref, pending, nfc_only) VALUES (?, ?, ?, ?, ?, ?)",
            (
                tag,
                name,
                notes,
                ref,
                pending,
                nfc_only
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_diacs (entries, db):
    for entry in entries:
        value = entry['value']
        name = sanitize (entry['name'])

        db.execute (
            "INSERT INTO diacs (value, name) VALUES (?, ?)",
            (
                value,
                name
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_cod_services (entries, db):
    for entry in entries:
        bit = entry['bit']
        name = sanitize (entry['name'])

        db.execute (
            "INSERT INTO cod_services (bit, name) VALUES (?, ?)",
            (
                bit,
                name
            )
        )

###############################################################################
###############################################################################
###############################################################################

def parse_cod_device_class (entries, db):
    for entry in entries:
        major = entry['major']
        name = sanitize (entry['name'])
        if 'subsplit' in entry:
            subsplit = entry['subsplit']
        else:
            subsplit = 0

        db.execute (
            "INSERT INTO cod_major (major, subsplit, name) VALUES (?, ?, ?)",
            (
                major,
                subsplit,
                name
            )
        )

        if 'minor' in entry:
            for minor_entry in entry['minor']:
                minor_value = minor_entry['value']
                minor_name = sanitize (minor_entry['name'])

                db.execute (
                    "INSERT INTO cod_minor (major, minor, name) VALUES (?, ?, ?)",
                    (
                        major,
                        minor_value,
                        minor_name
                    )
                )

        if 'minor_bits' in entry:
            for minor_entry in entry['minor_bits']:
                minor_value = minor_entry['value']
                minor_name = sanitize (minor_entry['name'])

                db.execute (
                    "INSERT INTO cod_minor_bits (major, minor, name) VALUES (?, ?, ?)",
                    (
                        major,
                        minor_value,
                        minor_name
                    )
                )

        if 'subminor' in entry:
            for minor_entry in entry['subminor']:
                minor_value = minor_entry['value']
                minor_name = sanitize (minor_entry['name'])

                db.execute (
                    "INSERT INTO cod_subminor (major, subminor, name) VALUES (?, ?, ?)",
                    (
                        major,
                        minor_value,
                        minor_name
                    )
                )

###############################################################################
###############################################################################
###############################################################################

def parse_appearance_values (data, db):
    for item in data:
        if 'category' in item and 'name' in item:
            category = int (item['category'])
            name = item['name']
            if 'state' in item:
                state = item['state']
            else:
                state = 'active'
            db.execute ("""
                INSERT INTO
                    appearance_categories
                    (category, state, name)
                VALUES
                    (?, ?, ?)""",
                (category, state, name)
            )

            if 'subcategory' in item:
                for subitem in item['subcategory']:
                    if 'value' in subitem and 'name' in subitem:
                        value = int (subitem['value'])
                        name = subitem['name']
                        if 'state' in subitem:
                            state = subitem['state']
                        else:
                            state = 'active'
                        db.execute ("""
                            INSERT INTO
                                appearance_subcategories
                                (category, subcategory, state, name)
                            VALUES
                                (?, ?, ?, ?)""",
                            (category, value, state, name)
                        )

###############################################################################
###############################################################################
###############################################################################

def parse_formattypes (entries, db):
    for entry in entries:
        value = int (entry['value'])
        short_name = entry['short_name']
        description = entry['description']
        exponent = entry['exponent']
        db.execute ("""
            INSERT INTO
                characteristic_presentation_format_types
                (value, short_name, description, exponent)
            VALUES
                (?, ?, ?, ?)""",
            (value, short_name, description, exponent)
        )

###############################################################################
###############################################################################
###############################################################################

def parse_yaml_file (filename, db):
    fp = open (filename, 'rb')
    data = fp.read ().decode ('utf8')
    fp.close ()
    content = yaml.load (data, Loader = yaml.Loader)
    if content is not None:
        if 'uuids' in content:
            parse_uuids (content['uuids'], db)
        if 'company_identifiers' in content:
            parse_company_identifiers (content['company_identifiers'], db)
        if 'hci_version' in content:
            parse_hci_version (content['hci_version'], db)
        if 'lmp_version' in content:
            parse_lmp_version (content['lmp_version'], db)
        if 'll_version' in content:
            parse_ll_version (content['ll_version'], db)
        if 'psms' in content:
            parse_psms (content['psms'], db)
        if 'ad_types' in content:
            parse_ad_types (content['ad_types'], db)
        if 'diacs' in content:
            parse_diacs (content['diacs'], db)
        if 'cod_services' in content:
            parse_cod_services (content['cod_services'], db)
        if 'cod_device_class' in content:
            parse_cod_device_class (content['cod_device_class'], db)
        if 'appearance_values' in content:
            parse_appearance_values (content['appearance_values'], db)
        if 'formattypes' in content:
            parse_formattypes (content['formattypes'], db)

###############################################################################
###############################################################################
###############################################################################

if __name__ == "__main__":

    dot = pathlib.Path ('.')

    try:
        os.unlink (dot / '_build' / 'assigned_numbers.db')
    except FileNotFoundError:
        pass

    db = sqlite3.connect (dot / '_build' / 'assigned_numbers.db')

    db.execute ("""
        CREATE TABLE uuids
        (
            uuid INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            uri TEXT,
            sdo TEXT,
            type TEXT,
            pending INTEGER
        )
    """)

    db.execute ("""
        CREATE TABLE compid
        (
            value INTEGER PRIMARY KEY,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE diacs
        (
            value INTEGER PRIMARY KEY,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE cod_service
        (
            bit INTEGER PRIMARY KEY,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE hci_version
        (
            value INTEGER PRIMARY KEY,
            name TEXT,
            pending INTEGER
        )
    """)

    db.execute ("""
        CREATE TABLE lmp_version
        (
            value INTEGER PRIMARY KEY,
            name TEXT,
            pending INTEGER
        )
    """)

    db.execute ("""
        CREATE TABLE ll_version
        (
            value INTEGER PRIMARY KEY,
            name TEXT,
            pending INTEGER
        )
    """)

    db.execute ("""
        CREATE TABLE psms
        (
            psm INTEGER PRIMARY KEY,
            name TEXT,
            ref TEXT,
            pending INTEGER
        )
    """)

    db.execute ("""
        CREATE TABLE cod_services
        (
            bit INTEGER PRIMARY KEY,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE cod_major
        (
            major INTEGER,
            subsplit INTEGER,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE cod_minor
        (
            major INTEGER,
            minor INTEGER,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE cod_minor_bits
        (
            major INTEGER,
            minor INTEGER,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE cod_subminor
        (
            major INTEGER,
            subminor INTEGER,
            name TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE ad_types
        (
            tag INTEGER,
            name TEXT,
            notes TEXT,
            ref TEXT,
            pending BOOLEAN,
            nfc_only BOOLEAN
        )
    """)

    db.execute ("""
        CREATE TABLE appearance_categories
        (
            category INTEGER PRIMARY KEY,
            name TEXT,
            state TEXT
        )
    """)

    db.execute ("""
        CREATE TABLE appearance_subcategories
        (
            category INTEGER,
            subcategory INTEGER,
            name TEXT,
            state TEXT
        )
    """)

    db.execute ("""
        CREATE INDEX
            appearance_subcategories_index
        ON
            appearance_subcategories
            (
                category,
                subcategory
            )
    """)

    db.execute ("""
        CREATE TABLE characteristic_presentation_format_types
        (
            value INTEGER,
            short_name TEXT,
            description TEXT,
            exponent BOOL
        )
    """)


    path = dot / '..' / 'assigned_numbers_doc' / 'yaml'
    assigned_number_files = os.listdir (path)

    for assigned_number_file in sorted (assigned_number_files):
        if assigned_number_file[-5:] == '.yaml':
            parse_yaml_file (path / assigned_number_file, db)

    db.commit ()

    db.execute ("""
        VACUUM
    """)

    db.commit ()

    db.close ()
