###############################################################################
###############################################################################
###############################################################################
##
## Copyright ⓒ 2019-2020 Qualcomm Innovation Center, Inc.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
## this list of conditions and the following disclaimer.
##
## 2. Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in the
## documentation and/or other materials provided with the distribution.
##
## 3. Neither the name of the copyright holder nor the names of its
## contributors may be used to endorse or promote products derived from this
## software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################
###############################################################################
###############################################################################

import hashlib
import os
import pathlib
import platform
import pprint
import re
import shutil
import sqlite3
import subprocess
import sys

###############################################################################
###############################################################################
###############################################################################

python_cmd = "python3"
if platform.system () == "Windows":
    python_cmd = "python.exe"

###############################################################################
###############################################################################
###############################################################################

cached_files = {}

###############################################################################
###############################################################################
###############################################################################

def is_interesting_file (filename, extensions):
    for ext in extensions:
        if filename[-len(ext):] == ext:
            return True
    return False

###############################################################################
###############################################################################
###############################################################################

def execute_block (block_file, content):
    command = python_cmd + ' ' + block_file
    content_hash = hashlib.sha256 ()
    content_hash.update (command.encode ('utf8'))
    content_hash.update (chr (0).encode ('utf8'))
    content_hash.update (content.encode ('utf8'))

    output_filename = dot / BUILD_DIRECTORY / CACHE_DIRECTORY / f"{content_hash.hexdigest()}.tex"

    input_filename = dot / CACHE_DIRECTORY / f"{content_hash.hexdigest()}.tex"

    input_filename = str (input_filename).replace ("\\", "/")

    if output_filename not in cached_files:
        proc = subprocess.Popen (command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            shell = True)

        output,_ = proc.communicate (content.encode ('utf8'))

        proc.wait ()

        if proc.returncode != 0:
            print (proc)
            print (output.decode ('utf8'))
            exit (1)

        with open (output_filename, "wb") as fp:
            fp.write (output)
        cached_files[output_filename] = True

    return f"\\input{{{input_filename}}} "

###############################################################################
###############################################################################
###############################################################################

index_data = {}
index_names = []
next_index = 1

def add_index (updated, entry):
    global next_index

    entry = entry.replace ("\n", " ")
    updated.append ("\\label{index:%d}" % next_index)
    index_names.append (entry)
    index_data[entry] = next_index
    next_index += 1

###############################################################################
###############################################################################
###############################################################################

def print_index (updated):
    updated.append ("\\input{index.tex}")

###############################################################################
###############################################################################
###############################################################################

def generate_index (filename):
    fp = open (filename, "wb")
    content = []
    for name in sorted (index_names):
        ni = index_data[name]
        content.append ("\\hyperref[index:%d]{%s, \\pageref{index:%d}\\\\}\n" % (ni, name, ni))
    fp.write (''.join (content).encode ('utf8'))
    fp.close ()

###############################################################################
###############################################################################
###############################################################################

def extract_index (content):
    normal = 0
    updated = []
    matches = re.finditer (r"(\\Index)\s*{([^}]+)}|(\\PrintIndex)", content, re.MULTILINE | re.DOTALL)
    for match in matches:
        start = match.start ();
        end = match.end ();
        updated.append (content[normal:start])
        if match.group (1) == '\\Index':
            add_index (updated, match.group (2))
        elif match.group (3) == '\\PrintIndex':
            print_index (updated)
        normal = end
    updated.append (content[normal:])
    return ''.join (updated)

###############################################################################
###############################################################################
###############################################################################

def preprocess_block (content):
    normal = 0
    updated = []
    matches = re.finditer (r"(^````)\s*(\S+\.py)(.*?)\1", content, re.MULTILINE | re.DOTALL)
    for match in matches:
        start = match.start ();
        end = match.end ();
        updated.append (content[normal:start])
        updated.append (execute_block (match.group (2), match.group (3)))
        normal = end
    updated.append (content[normal:])
    return ''.join (updated)

###############################################################################
###############################################################################
###############################################################################

def process_and_copy (from_path, to_path):
    with open (from_path) as from_fp:
        content = from_fp.read ()
        content = preprocess_block (content)
        content = extract_index (content)
        with open (to_path, "w") as to_fp:
            to_fp.write (content)

###############################################################################
###############################################################################
###############################################################################

def copy_updated_files (from_path, into_path, extensions, process = False):
    number_of_changed_files = 0

    files = os.listdir (from_path)
    for filename in files:
        if is_interesting_file (filename, extensions):
            path = from_path / pathlib.Path (filename)
            with open (path, "rb") as fp:
                m = hashlib.sha1 ()
                m.update (fp.read ())
                digest = m.digest ()
                if process:
                    process_and_copy (path, into_path / pathlib.Path (filename))
                else:
                    shutil.copy (path, into_path / pathlib.Path (filename))

###############################################################################
###############################################################################
###############################################################################

def calculate_hash (filename):
    try:
        fp = open (filename, "rb")
        if fp:
            m = hashlib.sha1 ()
            m.update (fp.read ())
            fp.close ()
            return m.digest ()
    except FileNotFoundError:
        pass

    return b''

###############################################################################
###############################################################################
###############################################################################

if __name__ == "__main__":

    TEMPLATE_DIRECTORY = "templates"
    MEDIA_DIRECTORY = "templates/media"
    BUILD_DIRECTORY = "_build"
    CACHE_DIRECTORY = "_cache"

    dot = pathlib.Path ('.')

    cache_path = dot / BUILD_DIRECTORY / CACHE_DIRECTORY
    cache_path.mkdir (parents = True, exist_ok = True)

    build_catalog = [
        (
            dot / '..' / 'templates' ,
            [ '.cls' ],
            dot / BUILD_DIRECTORY ,
            False
        ),
        (
            dot / '..' / 'templates' / 'media',
            [ '.eps', '.otf', '.ttf' ],
            dot / BUILD_DIRECTORY / 'media',
            False
        ),
        (
            dot / '..' / 'assigned_numbers_doc' / 'src',
            [ '.tex' ],
            dot / BUILD_DIRECTORY,
            True
        ),
        (
            dot / '..' / 'assigned_numbers_doc' / 'yaml',
            [ '.yaml' ],
            dot / BUILD_DIRECTORY,
            False
        ),
    ]

    index_filename = dot / BUILD_DIRECTORY / 'index.tex'

    fp = open (index_filename, "wb")
    fp.close ()

    for from_path, extensions, to_path, process in build_catalog:
        to_path.mkdir (parents = True, exist_ok = True)
        copy_updated_files (from_path, to_path, extensions, process)

    generate_index (index_filename)

# Compile the assigned_numbers.pdf using xelatex in the _build directory

    output_digest = calculate_hash (dot / BUILD_DIRECTORY / "assigned_numbers.out")

    for iteration in range (1, 6):
        cwd = os.getcwd ()

        os.chdir (dot / BUILD_DIRECTORY)

        cmd_args = [
            'xelatex',
            '-interaction=nonstopmode',
            '-halt-on-error',
            'assigned_numbers.tex'
        ]

        print (f"Compiling (Iteration {iteration})")
        proc = subprocess.run (cmd_args, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        if proc.returncode != 0:
            sys.stderr.write (proc.stdout.decode ('utf8'))
            os.chdir (cwd)
            exit (1)

        rerun = False
        if b'Rerun to get' in proc.stdout:
            rerun = True

        os.chdir (cwd)

        new_output_digest = calculate_hash (dot / BUILD_DIRECTORY / "assigned_numbers.out")
        
        if not rerun and output_digest == new_output_digest:
            break

        output_digest = new_output_digest

    if len (sys.argv) > 1:
        fp = open (sys.argv[1], "rb")
        content = fp.read ().decode ('utf8')
        fp.close ()

        before, other = content.split ('{r')
        inside, after = other.split ('}')

        revision = int (inside)

        filename = "assigned_numbers_r%02d.pdf" % revision

        shutil.copy (dot / BUILD_DIRECTORY / "assigned_numbers.pdf", filename)
        print ("Complete", filename)

    else:
        shutil.copy (dot / BUILD_DIRECTORY / "assigned_numbers.pdf", "assigned_numbers.pdf")
        print ("Complete", "assigned_numbers.pdf")


