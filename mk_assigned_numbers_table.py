###############################################################################
###############################################################################
###############################################################################
##
## Copyright ⓒ 2019-2021 Qualcomm Innovation Center, Inc.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
## this list of conditions and the following disclaimer.
##
## 2. Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in the
## documentation and/or other materials provided with the distribution.
##
## 3. Neither the name of the copyright holder nor the names of its
## contributors may be used to endorse or promote products derived from this
## software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################
###############################################################################
###############################################################################

import io
import os
import pathlib
import sqlite3
import sys

###############################################################################
###############################################################################
###############################################################################

generate_draft = False

###############################################################################
###############################################################################
###############################################################################

sys.stdout = io.TextIOWrapper (sys.stdout.detach (), encoding = 'utf8')
sys.stderr = io.TextIOWrapper (sys.stderr.detach (), encoding = 'utf8')

###############################################################################
###############################################################################
###############################################################################

def safe_latex (unsafe):
    safe = unsafe.replace ('\\-', '⎄')
    safe = safe.replace ('\\', "\\textbackslash{}")
    safe = safe.replace ('{', "\\{")
    safe = safe.replace ('}', "\\}")
    safe = safe.replace ('$', "\\$")
    safe = safe.replace ('&', "\\&")
    safe = safe.replace ('%', "\\%")
    safe = safe.replace ('#', "\\#")
    safe = safe.replace ('_', "\\_")
    safe = safe.replace ('–', " --- ")
    safe = safe.replace ('–', " -- ")
    safe = safe.replace ('~ ', "\\textasciitilde\\ ")
    safe = safe.replace ('~', "\\textasciitilde{}")
    safe = safe.replace ('^', "\\textasciicircum{}")
    safe = safe.replace ('\n', '\n\n')
    safe = safe.replace ('⎄', '\\-')
    return safe.strip ()

###############################################################################
###############################################################################
###############################################################################

def output_table (table, table_format, header):
    print ("\\vspace{6pt}")
    # print ("\\makebox[\\textwidth]{")
    # print ("\\makebox[0pt][c]{\color{red}\\rule[-28pt]{0.4pt}{32pt}}\\hspace{-2.6pt}")
    # print ("\\color{blue}\\rule{\\textwidth}{0.4pt}")
    # print ("\\hspace{-2.6pt}\\makebox[0pt][c]{\color{red}\\rule[-28pt]{0.4pt}{32pt}}")
    # print ("}\\vspace{-32pt}")
    print ("\\StartTable{" + table_format + "}\n")
    print ("\\StartHeaderRow")
    first = True
    for item in header:
        if first:
            first = False
        else:
            print (" & ")
        print ("\\HeaderCell {" + item + "}")
    print ("\\EndHeaderRow\n")

    need_end_table_row = False

    for row in table:
        if need_end_table_row:
            if row[0] == "":
                print ("\\EndCTableRow{2-%d}\n" % len (row))
            else:
                print ("\\EndTableRow\n")
        print ("\\StartTableRow")
        need_end_table_row = True
        first = True
        for item in row:
            if first:
                first = False
            else:
                print (" & ")
            print ("\\TableCell {" + item + "}")
    if need_end_table_row:
        print ("\\EndTableRow\n")

    print ("\\EndTable\n")
    

###############################################################################
###############################################################################
###############################################################################

def output_uuid_table_by_uuid (uuid_type, header):
    table = []

    if isinstance (uuid_type, str):
        rows = db.execute ("""
            SELECT
                uuid, name, pending
            FROM
                uuids
            WHERE
                type = ?
            ORDER BY
                uuid
        """, (uuid_type, ))
    else:
        items = ', '.join (len (uuid_type) * "?")
        query = f"""
            SELECT
                uuid, name, pending
            FROM
                uuids
            WHERE
                type in ({items})
            ORDER BY
                uuid
        """

        rows = db.execute (query, (uuid_type))

    for row in rows:
        uuid, name, pending = row
        if not pending:
            table.append ((
                f"\\texttt{{0x{uuid:04X}}}",
                safe_latex (name)
            ))

    output_table (table, "L{2cm} L{14cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_uuid_table_by_name (uuid_type, header):
    table = []

    if isinstance (uuid_type, str):
        rows = db.execute ("""
            SELECT
                uuid, name, pending
            FROM
                uuids
            WHERE
                type = ?
            ORDER BY
                name COLLATE NOCASE, uuid
        """, (uuid_type, ))
    else:
        items = ', '.join (len (uuid_type) * "?")
        query = f"""
            SELECT
                uuid, name, pending
            FROM
                uuids
            WHERE
                type in ({items})
            ORDER BY
                name COLLATE NOCASE, uuid
        """
        rows = db.execute (query, (uuid_type))

    for row in rows:
        uuid, name, pending = row
        if not pending:
            table.append ((
                safe_latex (name),
                f"\\texttt{{0x{uuid:04X}}}"
            ))

    output_table (table, "L{14.15cm} C{2cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_uuid_uri_table_by_name (uuid_type, header):
    table = []

    if isinstance (uuid_type, str):
        rows = db.execute ("""
            SELECT
                uuid, uri, name, pending
            FROM
                uuids
            WHERE
                type = ?
            ORDER BY
                name COLLATE NOCASE, uuid
        """, (uuid_type, ))
    else:
        items = ', '.join (len (uuid_type) * "?")
        query = f"""
            SELECT
                uuid, uri, name, pending
            FROM
                uuids
            WHERE
                type in ({items})
            ORDER BY
                name COLLATE NOCASE, uuid
        """
        rows = db.execute (query, (uuid_type))

    for row in rows:
        uuid, uri, name, pending = row
        safe_name = safe_latex (name)
        safe_uri = safe_latex (uri)
        safe_uri = safe_uri.replace (".", ".\\hspace{0pt}")
        if not pending:
            table.append ((
                safe_name + " \\newline\\hspace{1cm}(" + safe_uri + ")",
                f"\\texttt{{0x{uuid:04X}}}"
            ))

    header = [header[0] + " \\newline\\hspace{1cm}(" + header[1] + ")", header[2]]
    output_table (table, "L{14.15cm} C{2cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_protocol_identifiers_by_uuid (arg):
    header = ("UUID", "Protocol Name")
    output_uuid_table_by_uuid ('protocol_identifier', header)

###############################################################################
###############################################################################
###############################################################################

def output_service_classes_by_name (arg):
    table = []

    rows = db.execute ("""
        SELECT
            uuid, type, name
        FROM
            uuids
        WHERE
            type = 'service_class' OR
            type = 'profile' OR
            type = 'service_class_and_profile'
        ORDER BY
            name
    """)

    for row in rows:
        uuid, uuid_type, name = row

        if uuid_type == 'service_class': scop = 'Service Class'
        elif uuid_type == 'profile': scop = 'Profile'
        elif uuid_type == 'service_class_and_profile': scop = 'Service Class / Profile'
        else: scop = 'Unknown'

        table.append ((
            safe_latex (name),
            safe_latex (scop),
            f"\\texttt{{0x{uuid:04X}}}"
        ))

    header = ("Name", "Usage", "UUID")
    output_table (table, "L{6.95cm} L{6.95cm} C{2cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_public_browse_group_by_name (arg):
    header = ("Browse Group Name", "UUID")
    output_uuid_table_by_name ('browse_group', header)

###############################################################################
###############################################################################
###############################################################################

def output_protocol_identifiers_by_name (arg):
    header = ("Protocol Name", "UUID")
    output_uuid_table_by_name ('protocol_identifier', header)

###############################################################################
###############################################################################
###############################################################################

def output_gatt_services_by_uuid (arg):
    header = ("UUID", "Service Name")
    output_uuid_table_by_uuid ('service', header)

###############################################################################
###############################################################################
###############################################################################

def output_gatt_services_by_name (arg):
    header = ("Service Name", "URI", "UUID")
    output_uuid_uri_table_by_name ('service', header)

###############################################################################
###############################################################################
###############################################################################

def output_units_by_uuid (arg):
    header = ("UUID", "Unit Name")
    output_uuid_table_by_uuid ('unit', header)

###############################################################################
###############################################################################
###############################################################################

def output_units_by_name (arg):
    header = ("Unit Name", "URI", "UUID")
    output_uuid_uri_table_by_name ('unit', header)

###############################################################################
###############################################################################
###############################################################################

def output_declarations_by_uuid (arg):
    header = ("UUID", "Declaration Name")
    output_uuid_table_by_uuid ('declaration', header)

###############################################################################
###############################################################################
###############################################################################

def output_declarations_by_name (arg):
    header = ("Declaration Name", "URI", "UUID")
    output_uuid_uri_table_by_name ('declaration', header)

###############################################################################
###############################################################################
###############################################################################

def output_descriptors_by_uuid (arg):
    header = ("UUID", "Descriptor Name")
    output_uuid_table_by_uuid ('descriptor', header)

###############################################################################
###############################################################################
###############################################################################

def output_descriptors_by_name (arg):
    header = ("Descriptor Name", "URI", "UUID")
    output_uuid_uri_table_by_name ('descriptor', header)

###############################################################################
###############################################################################
###############################################################################

def output_characteristics_by_uuid (arg):
    header = ("UUID", "Characteristic Name")
    output_uuid_table_by_uuid ('characteristic', header)

###############################################################################
###############################################################################
###############################################################################

def output_characteristics_by_name (arg):
    header = ("Characteristic Name", "URI", "UUID")
    output_uuid_uri_table_by_name ('characteristic', header)

###############################################################################
###############################################################################
###############################################################################

def output_object_types_by_name (arg):
    header = ("Object Type Name", "URI", "UUID")
    output_uuid_uri_table_by_name ('object', header)

###############################################################################
###############################################################################
###############################################################################

def output_member_services_by_uuid (arg):
    header = ("UUID", "Member Name")
    output_uuid_table_by_uuid ('member_service', header)

###############################################################################
###############################################################################
###############################################################################

def output_member_services_by_name (arg):
    header = ("Member Name", "UUID")
    output_uuid_table_by_name ('member_service', header)

###############################################################################
###############################################################################
###############################################################################

def output_sdo_services_by_uuid (arg):
    table = []

    rows = db.execute ("""
        SELECT
            uuid, sdo, name
        FROM
            uuids
        WHERE
            type = 'sdo_service'
        ORDER BY
            uuid, name, sdo
    """)

    for row in rows:
        uuid, sdo, name = row
        table.append ((
            f"\\texttt{{0x{uuid:04X}}}",
            safe_latex (sdo),
            safe_latex (name)
        ))

    header = ("UUID", "SDO", "Service Name")
    output_table (table, "L{2cm} L{7cm} L{7cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_sdo_services_by_name (arg):
    table = []

    rows = db.execute ("""
        SELECT
            uuid, sdo, name
        FROM
            uuids
        WHERE
            type = 'sdo_service'
        ORDER BY
            name, sdo
    """)

    for row in rows:
        uuid, sdo, name = row
        table.append ((
            safe_latex (name),
            safe_latex (sdo),
            f"\\texttt{{0x{uuid:04X}}}"
        ))

    header = ("Service Name", "SDO", "UUID")
    output_table (table, "L{6.95cm} L{6.95cm} L{2cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_uuid_summary (arg):
    table = []

    rows = db.execute ("""
        SELECT
            uuid, type, name, pending
        FROM
            uuids
        ORDER BY
            uuid
    """)

    for row in rows:
        uuid, uuid_type, name, pending = row
        if not pending:
            if uuid_type == 'service':
                ref = '\\Cref{sec:services}'
            elif uuid_type == 'service_class':
                ref = '\\Cref{sec:service_class}'
            elif uuid_type == 'browse_group':
                ref = '\\Cref{sec:browse_group_identifiers}'
            elif uuid_type == 'profile':
                ref = '\\Cref{sec:service_class}'
            elif uuid_type == 'service_class_and_profile':
                ref = '\\Cref{sec:service_class}'
            elif uuid_type == 'member_service':
                ref = '\\Cref{sec:member_services}'
            elif uuid_type == 'sdo_service':
                ref = '\\Cref{sec:sdo_services}'
            elif uuid_type == 'descriptor':
                ref = '\\Cref{sec:descriptors}'
            elif uuid_type == 'uuid':
                ref = '\\Cref{sec:uuids}'
            elif uuid_type == 'unit':
                ref = '\\Cref{sec:units}'
            elif uuid_type == 'declaration':
                ref = '\\Cref{sec:declarations}'
            elif uuid_type == 'protocol_identifier':
                ref = '\\Cref{sec:protocol_identifier}'
            elif uuid_type == 'characteristic':
                ref = '\\Cref{sec:characteristics}'
            elif uuid_type == 'object':
                ref = '\\Cref{sec:object_types}'
            else:
                ref = ''
            table.append ((
                f"\\texttt{{0x{uuid:04X}}}",
                safe_latex (name),
                ref
            ))

    header = ("UUID", "Name", "Reference")
    output_table (table, "L{2cm} L{10.95cm} L{3cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_lmp_version_numbers (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, name
        FROM
            lmp_version
        ORDER BY
            value
    """)

    for row in rows:
        value, name = row
        table.append ((
            f"0x{value:02X}",
            safe_latex (name)
        ))

    header = ("LMP Version Number", "Name")
    output_table (table, "L{3cm} L{13cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_hci_version_numbers (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, name
        FROM
            hci_version
        ORDER BY
            value
    """)

    for row in rows:
        value, name = row
        table.append ((
            f"\\texttt{{0x{value:02X}}}",
            safe_latex (name)
        ))

    header = ("HCI Version Number", "Name")
    output_table (table, "L{3cm} L{13cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_ll_version_numbers (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, name
        FROM
            ll_version
        ORDER BY
            value
    """)

    for row in rows:
        value, name = row
        table.append ((
            f"\\text{{0x{value:02X}}}",
            safe_latex (name)
        ))

    header = ("LL Version Number", "Name")
    output_table (table, "L{3cm} L{13cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_company_identifiers_by_value (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, name
        FROM
            compid
        ORDER BY
            value
    """)

    for row in rows:
        value, name = row
        table.append ((
            f"\\texttt{{0x{value:04X}}}",
            safe_latex (name)
        ))

    header = ("Company Identifier", "Company Name")
    output_table (table, "C{4cm} L{12cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_company_identifiers_by_name (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, name
        FROM
            compid
        ORDER BY
            name COLLATE NOCASE
    """)

    for row in rows:
        value, name = row
        table.append ((
            safe_latex (name),
            f"\\texttt{{0x{value:04X}}}"
        ))

    header = ("Company Name", "Company Identifier")
    output_table (table, "L{14.15cm} C{2cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_ad_types_by_tag (arg):
    table = []

    rows = db.execute ("""
        SELECT
            tag, name, ref
        FROM
            ad_types
        WHERE
            NOT pending
        ORDER BY
            tag
    """)

    for row in rows:
        tag, name, ref = row
        table.append ((
            f"\\texttt{{0x{tag:02X}}}",
            safe_latex (name),
            safe_latex (ref)
        ))

    header = ("Data Type", "Name", "Reference")
    output_table (table, "L{2cm} L{4cm} L{9.95cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_ad_types_by_name (arg):
    table = []

    rows = db.execute ("""
        SELECT
            tag, name, notes, ref
        FROM
            ad_types
        WHERE
            NOT pending
        ORDER BY
            name COLLATE NOCASE
    """)

    for row in rows:
        tag, name, notes, ref = row
        if type (notes) == str:
            table.append ((
                safe_latex (name),
                f"\\texttt{{0x{tag:02X}}}",
                safe_latex (ref) + "\\par (" + safe_latex (notes) + ")",
            ))
        else:
            table.append ((
                safe_latex (name),
                f"\\texttt{{0x{tag:02X}}}",
                safe_latex (ref)
            ))

    header = ("Name", "Type", "Reference and Notes")
    output_table (table, "L{4cm} C{2cm} L{9.95cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_psms (arg):
    table = []

    rows = db.execute ("""
        SELECT
            psm, name, ref
        FROM
            psms
        WHERE
            NOT pending
        ORDER BY
            psm
    """)

    for row in rows:
        psm, name, ref = row
        table.append ((
            f"\\hfill \\texttt{{0x{psm:04X}}} \\hfill",
            safe_latex (name),
            safe_latex (ref)
        ))

    header = ("Protocol Service Multiplex (PSM)", "Name", "Reference")
    output_table (table, "L{3cm} L{4cm} L{8.95cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_diacs (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, name
        FROM
            diacs
        ORDER BY
            value
    """)

    for row in rows:
        value, name = row
        table.append ((
            f"\\texttt{{0x{value:04X}}}",
            safe_latex (name)
        ))

    header = ("LAP Value", "Usage")
    output_table (table, "L{4cm} L{12cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_cod_service (arg):
    table = []

    rows = db.execute ("""
        SELECT
            bit, name
        FROM
            cod_services
        ORDER BY
            bit
    """)

    for row in rows:
        bit, name = row
        table.append ((
            f"{bit}",
            safe_latex (name)
        ))

    header = ("Service Class Bit", "Name")
    output_table (table, "L{4cm} L{12cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_characteristic_presentation_format_types (arg):
    table = []

    rows = db.execute ("""
        SELECT
            value, short_name, description, exponent
        FROM
            characteristic_presentation_format_types
        ORDER BY
            value
    """)

    for row in rows:
        value, short_name, description, exponent = row
        if exponent:
            exponent_value = "Yes"
        else:
            exponent_value = "No"

        table.append ((
            f"\\texttt{{0x{value:02X}}}",
            safe_latex (short_name),
            safe_latex (description),
            exponent_value
        ))

    header = ("Format", "Short Name", "Description", "Exponent Value")
    output_table (table, "L{3cm} L{3cm} L{7cm} L{3cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_cod_major (arg):
    table = []

    rows = db.execute ("""
        SELECT
            major, name
        FROM
            cod_major
        ORDER BY
            major
    """)

    for row in rows:
        major, name = row
        table.append ((
            f"{major}",
            safe_latex (name)
        ))

    header = ("Major Device Class (bits 12 to 8)", "Name")
    output_table (table, "L{4cm} L{12cm}", header)

###############################################################################
###############################################################################
###############################################################################

def output_cod_minor (arg):
    table = []

    major = arg[0]

    rows = db.execute ("""
        SELECT
            subsplit, name
        FROM
            cod_major
        WHERE
            major = ?
    """, (major, ))

    for row in rows:
        subsplit, major_name = row

    rows = db.execute ("""
        SELECT
            minor, name
        FROM
            cod_minor
        WHERE
            major = ?
        ORDER BY
            minor
    """, (major, ))

    for row in rows:
        minor, name = row
        if subsplit != 0:
            table.append ((
                f"{minor}",
                safe_latex (name)
            ))
        else:
            table.append ((
                f"\\texttt{{0x{minor:02X}}}",
                safe_latex (name)
            ))

    if len (table) > 0:
        if subsplit != 0:
            major_name = safe_latex (major_name)
            sub = 8 - subsplit
            header = (f"Minor Device Class Bits 7 to {sub}", "Name")
        else:
            header = (f"Minor Device Class", "Name")
        output_table (table, "L{4cm} L{12cm}", header)

    rows = db.execute ("""
        SELECT
            minor, name
        FROM
            cod_minor_bits
        WHERE
            major = ?
        ORDER BY
            minor
    """, (major, ))

    table = []

    for row in rows:
        minor, name = row
        table.append ((
            f"{minor}",
            safe_latex (name)
        ))

    if len (table) > 0:
        header = (f"Minor Device Class Bit", "Name")
        output_table (table, "L{4cm} L{12cm}", header)

    rows = db.execute ("""
        SELECT
            subminor, name
        FROM
            cod_subminor
        WHERE
            major = ?
        ORDER BY
            subminor
    """, (major, ))

    table = []

    for row in rows:
        subminor, name = row
        table.append ((
            f"{subminor:02X}",
            safe_latex (name)
        ))

    if len (table) > 0:
        sub = 7 - subsplit
        header = (f"Minor Device Class Bits {sub} to 2", "Name")
        output_table (table, "L{4cm} L{12cm}", header)


###############################################################################
###############################################################################
###############################################################################

def output_appearance_categories (arg):
    table = []

    last_category = -1

    rows = db.execute ("""
        SELECT
            category, name, state
        FROM
            appearance_categories
        ORDER BY
            category
    """)

    for row in rows:
        category, name, state = row
        last_subcategory = 0

        start = category * 64
        end = category * 64 + 63
        if generate_draft:
            if state == 'new':
                table.append ((
                    f"\\color{{BluetoothRed}}\\texttt{{0x{category:03X}}}",
                    f"\\color{{BluetoothRed}}\\texttt{{0x{start:04X}}} to \\texttt{{0x{end:04X}}}",
                    "\\color{BluetoothRed}" + safe_latex (name)
                ))
            elif state == 'proposed':
                table.append ((
                    f"\\color{{BluetoothGreen}}\\texttt{{0x{category:03X}}}",
                    f"\\color{{BluetoothGreen}}\\texttt{{0x{start:04X}}} to \\texttt{{0x{end:04X}}}",
                    "\\color{BluetoothGreen}" + safe_latex (name)
                ))
            else:
                table.append ((
                    f"\\texttt{{0x{category:03X}}}",
                    f"\\texttt{{0x{start:04X}}} to \\texttt{{0x{end:04X}}}",
                    safe_latex (name)
                ))
        else:
            table.append ((
                f"\\texttt{{0x{category:03X}}}",
                f"\\texttt{{0x{start:04X}}} to \\texttt{{0x{end:04X}}}",
                safe_latex (name)
            ))

        last_category = category

    header = ("Category (bits 15 to 6)", "Value Ranges", "Name")
    output_table (table, "| C{4cm} | C{4.95cm} | L{7cm} |", header)

###############################################################################
###############################################################################
###############################################################################

def output_appearance_values (arg):
    table = []

    last_category = -1

    rows = db.execute ("""
        SELECT
            category, name, state
        FROM
            appearance_categories
        ORDER BY
            category
    """)

    for row in rows:
        category, name, state = row
        last_subcategory = 0

        start = category * 64
        end = category * 64 + 63
        if generate_draft:
            if state == 'new':
                table.append ((
                    f"\\color{{BluetoothRed}}\\texttt{{0x{category:03X}}}",
                    "\\color{BluetoothRed}\\texttt{0x00}",
                    f"\\color{{BluetoothRed}}\\texttt{{0x{start:04X}}}",
                    "\\color{BluetoothRed}Generic " + safe_latex (name)
                ))
            elif state == 'proposed':
                table.append ((
                    f"\\color{{BluetoothGreen}}\\texttt{{0x{category:03X}}}",
                    "\\color{BluetoothGreen}\\texttt{0x00}",
                    f"\\color{{BluetoothGreen}}\\texttt{{0x{start:04X}}}",
                    "\\color{BluetoothGreen}Generic " + safe_latex (name)
                ))
            else:
                table.append ((
                    f"\\texttt{{0x{category:03X}}}",
                    "\\texttt{0x00}",
                    f"\\texttt{{0x{start:04X}}}",
                    "Generic " + safe_latex (name)
                ))
        else:
            table.append ((
                f"\\texttt{{0x{category:03X}}}",
                "\\texttt{0x00}",
                f"\\texttt{{0x{start:04X}}}",
                "Generic " + safe_latex (name)
            ))

        subrows = db.execute ("""
            SELECT
                subcategory, name, state
            FROM
                appearance_subcategories
            WHERE
                category = ?
            ORDER BY
                subcategory
        """, (category, ))

        for subrow in subrows:
            subcategory, subname, substate = subrow

            last_subcategory = subcategory
            value = category * 64 + subcategory

            if generate_draft:
                if substate == 'new':
                    table.append ((
                        f"",
                        f"\\color{{BluetoothRed}}\\texttt{{0x{subcategory:02X}}}",
                        f"\\color{{BluetoothRed}}\\texttt{{0x{value:04X}}}",
                        "\\color{BluetoothRed}" + safe_latex (subname)
                    ))
                elif substate == 'proposed':
                    table.append ((
                        f"",
                        f"\\color{{BluetoothGreen}}\\texttt{{0x{subcategory:02X}}}",
                        f"\\color{{BluetoothGreen}}\\texttt{{0x{value:04X}}}",
                        "\\color{BluetoothGreen}" + safe_latex (subname)
                    ))
                else:
                    table.append ((
                        f"",
                        f"\\texttt{{0x{subcategory:02X}}}",
                        f"\\texttt{{0x{value:04X}}}",
                        safe_latex (subname)
                    ))
            else:
                table.append ((
                    f"",
                    f"\\texttt{{0x{subcategory:02X}}}",
                    f"\\texttt{{0x{value:04X}}}",
                    safe_latex (subname)
                ))

        last_category = category

    header = ("{Category \\newline (bits 15 to 6)}", "Subcatgeory (bits 5 to 0)", "Value", "Name")
    output_table (table, "| C{2.75cm} | C{2.75cm} | C{3.25cm} | L{7cm} |", header)

###############################################################################
###############################################################################
###############################################################################

supported_words = {
    'protocol_identifiers_by_uuid': output_protocol_identifiers_by_uuid,
    'protocol_identifiers_by_name': output_protocol_identifiers_by_name,
    'service_classes_by_name': output_service_classes_by_name,
    'public_browse_group_by_name': output_public_browse_group_by_name,
    'gatt_services_by_uuid': output_gatt_services_by_uuid,
    'gatt_services_by_name': output_gatt_services_by_name,
    'units_by_uuid': output_units_by_uuid,
    'units_by_name': output_units_by_name,
    'declarations_by_uuid': output_declarations_by_uuid,
    'declarations_by_name': output_declarations_by_name,
    'descriptors_by_uuid': output_descriptors_by_uuid,
    'descriptors_by_name': output_descriptors_by_name,
    'characteristics_by_uuid': output_characteristics_by_uuid,
    'characteristics_by_name': output_characteristics_by_name,
    'object_types_by_name': output_object_types_by_name,
    'member_services': output_member_services_by_uuid,
    'sdo_services': output_sdo_services_by_uuid,
    'member_services_by_name': output_member_services_by_name,
    'sdo_services_by_name': output_sdo_services_by_name,
    'uuid_summary': output_uuid_summary,
    'lmp_version_numbers': output_lmp_version_numbers,
    'hci_version_numbers': output_hci_version_numbers,
    'll_version_numbers': output_ll_version_numbers,
    'company_identifiers_by_value': output_company_identifiers_by_value,
    'company_identifiers_by_name': output_company_identifiers_by_name,
    'ad_types_by_tag': output_ad_types_by_tag,
    'ad_types_by_name': output_ad_types_by_name,
    'psms': output_psms,
    'diacs': output_diacs,
    'cod_service': output_cod_service,
    'cod_major': output_cod_major,
    'cod_minor': output_cod_minor,
    'appearance_categories': output_appearance_categories,
    'appearance_values': output_appearance_values,
    'characteristic_presentation_format_types': output_characteristic_presentation_format_types,
}

###############################################################################
###############################################################################
###############################################################################

if __name__ == "__main__":
    dot = pathlib.Path ('.')

    db = sqlite3.connect (dot / "_build" / "assigned_numbers.db")

    content = sys.stdin.read ()

    for line in content.splitlines ():
        line = line.strip ()
        if line:
            words = line.split ()
            command = words[0]
            args = words[1:]
            if command in supported_words:
                supported_words[command] (args)
            else:
                print (f"Unsupported mk_assigned_numbers_table.py command «{safe_latex (command)}»")

###############################################################################
###############################################################################
###############################################################################
