###############################################################################
###############################################################################
###############################################################################
##
## Copyright ⓒ 2019-2020 Qualcomm Innovation Center, Inc.
##
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
##
## 1. Redistributions of source code must retain the above copyright notice,
## this list of conditions and the following disclaimer.
##
## 2. Redistributions in binary form must reproduce the above copyright
## notice, this list of conditions and the following disclaimer in the
## documentation and/or other materials provided with the distribution.
##
## 3. Neither the name of the copyright holder nor the names of its
## contributors may be used to endorse or promote products derived from this
## software without specific prior written permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
## INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
## ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
## POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################
###############################################################################
###############################################################################

import sqlite3
import pathlib
import pandas
import pprint

###############################################################################
###############################################################################
###############################################################################

def full_strip (s):
	s = s.replace('\u00A0', ' ')
	s = s.replace('\u200B', '')
	s = s.replace('\u200C', '')
	s = s.replace('\u200D', '')
	s = s.strip ()
	return s

###############################################################################
###############################################################################
###############################################################################

def show_all_database_tables (db):
	rows = db.execute ("SELECT * FROM sqlite_master")
	for row in rows:
		_, name, _, _, schema = row
		print (name)
		print (schema)

###############################################################################
###############################################################################
###############################################################################

def cross_check_16_bit_uuids (db):
	global num_errors
	global num_items
	global num_yaml_errors
	global num_xlsx_errors

	rows = db.execute ("SELECT uuid, name, sdo, type FROM UUIDS ORDER BY uuid")
	db_uuids = {}
	for row in rows:
		uuid, name, sdo, uuid_type = row
		uuid = int (uuid)
		db_uuids[uuid] = (name, sdo, uuid_type)
	
	type_map = {
		'Browse Group' : 'browse_group',
		'Service Classes and Profiles' : 'service_class',
		'GATT Service' : 'service',
		'Protocol Identifier' : 'protocol_identifier',
		'GATT Unit ' : 'unit',
		'Member GATT Service' : 'member_service',
		'SDO GATT Service' : 'sdo_service',
		'GATT Characteristic and Object Type' : 'characteristic',
		'GATT Descriptor' : 'descriptor',
		'GATT Declerations ' : 'declaration',
	}

	data_file = pandas.read_excel ("16-Bit UUIDs.xlsx", sheet_name = None)

	if 'Value Matrix' not in data_file:
		print ("Missing the 'Value Matrix' sheet in '16-Bit UUIDs.xlsx' file")
		exit (1)
	
	ignore_uuids = {}

	rows = data_file['Value Matrix'].to_dict ('records')
	xl_uuids = {}
	for row in rows:
		try:
			uuid = int (row['UUID'], 16)
			if type (row['Allocated Item ']) == float:
				print (f"ERROR: Name invalid for UUID 0x{uuid:04x}")
				num_errors += 1
				num_xlsx_errors += 1
				ignore_uuids[uuid] = True
			else:
				name = str (row['Allocated Item ']).strip ()
				uuid_type = str (row['Allocated for '])
				if uuid_type in type_map:
					uuid_type = type_map[uuid_type]
				sdo = None
				if uuid in xl_uuids:
					print (f"ERROR: Duplicate UUID {uuid:04x} '{name}' {xl_uuids[uuid][1]}")
					num_errors += 1
					num_xlsx_errors += 1
					ignore_uuids[uuid] = True
				else:
					xl_uuids[uuid] = (name, sdo, uuid_type)
		except:
			if row['UUID'] == '<RFU>' or row['UUID'] == '<RFU':
				print (f"ERROR: UUID invalid '{row['UUID']}' {row}")
				num_errors += 1
				num_xlsx_errors += 1
			else:
				raise

	for uuid in range (0, 2**16):
		if uuid not in ignore_uuids and (uuid in db_uuids or uuid in xl_uuids):
			num_items += 1
			if uuid in db_uuids and uuid in xl_uuids:
				db_name, db_sdo_name, db_type = db_uuids[uuid]
				xl_name, xl_sdo_name, xl_type = xl_uuids[uuid]
				if db_name != xl_name and db_sdo_name != xl_name:
					print (f"ERROR: 0x{uuid:04x}", end = ' : ')
					print ("Name Mismatch", end = ' : ')
					print (f"'{db_name}' != '{xl_name}'")
					print (f"ERROR: 0x{uuid:04x}", end = ' : ')
					print ("Name Mismatch", end = ' : ')
					print (f"'{db_sdo_name}' != '{xl_name}'")
					num_errors += 1
					num_xlsx_errors += 1
					num_yaml_errors += 1
				if db_type != xl_type:
					if db_type == 'profile' and xl_type == 'service_class':
						pass
					elif db_type == 'service_class_and_profile' and xl_type == 'service_class':
						pass
					elif db_type == 'object' and xl_type == 'characteristic':
						pass
					else:
						print (f"ERROR: 0x{uuid:04x}", end = ' : ')
						print ("Type Mismatch", end = ' : ')
						print (f"'{db_type}' != '{xl_type}'")
						num_errors += 1
						num_xlsx_errors += 1
						num_yaml_errors += 1
			else:
				if uuid in db_uuids:
					print (f"ERROR: 0x{uuid:04x}", end = ' : ')
					print ("Missing from xlsx", end = ' : ')
					print (db_uuids[uuid])
					num_errors += 1
					num_xlsx_errors += 1
				elif uuid in xl_uuids:
					print (f"ERROR: 0x{uuid:04x}", end = ' : ')
					print ("Missing from yaml", end = ' : ')
					print (xl_uuids[uuid])
					num_errors += 1
					num_yaml_errors += 1

###############################################################################
###############################################################################
###############################################################################

def cross_check_ad_types (db):
	global num_errors
	global num_items
	global num_yaml_errors
	global num_xlsx_errors

	rows = db.execute ("SELECT tag, name, ref, pending, nfc_only FROM ad_types ORDER BY tag")
	ad_types = {}
	nfc_ad_types = {}
	for row in rows:
		tag, name, ref, pending, nfc_only = row
		num_items += 1
		if nfc_only:
			if tag in nfc_ad_types:
				print (f"ERROR: Duplicate AD_Type 0x{tag:02x} '{name}' '{nfc_ad_types[tag][0]}'")
				num_errors += 1
			else:
				nfc_ad_types[tag] = (name, ref, pending, nfc_only)
		else:
			if tag in ad_types:
				print (f"ERROR: Duplicate AD_Type 0x{tag:02x} '{name}' '{ad_types[tag][0]}'")
				num_errors += 1
			else:
				ad_types[tag] = (name, ref, pending, nfc_only)

	data_file = pandas.read_excel ("AD Type.xlsx", sheet_name = None)

	rows = data_file['AD Types'].to_dict ('records')

	xl_ad_types = []

	for row in rows:
		tag = row['Data Type Value']
		if isinstance (tag, str):
			tag = full_strip (tag)
			tag = int (tag, 16)
			name = row['Data Type Name']
			if isinstance (name, str):
				name = full_strip (name)
				if name[0] == '«':
					name = name[1:]
				if name[-1] == '»':
					name = name[:-1]
			else:
				name = ''
			ref = row['Reference for Definition']
			if isinstance (ref, str):
				ref = full_strip (ref)
			else:
				ref = ''
			xl_ad_types.append ((tag, name, ref))

	for tag, name, ref in xl_ad_types:
		missing = True # Assume the worst
		good = False # Assume the worst
		differ = False # Assume the worst
		if tag in ad_types:
			missing = False
			if name == ad_types[tag][0]:
				good = True
			if ref in ad_types[tag][1]:
				differ = True
		if tag in nfc_ad_types:
			missing = False
			if name == nfc_ad_types[tag][0]:
				good = True
			if ref in nfc_ad_types[tag][1]:
				differ = True
		if missing:
			num_errors += 1
			print (f"ERROR: Tag missing 0x{tag:02x} from xlsx")
		else:
			if not good:
				print (f"ERROR: Name mismatch 0x{tag:02x}")
				if tag in ad_types:
					print (f"  \"{name}\" != \"{ad_types[tag][0]}\"")
				else:
					print (f"  \"{name}\" != \"{nfc_ad_types[tag][0]}\"")
			if not differ:
				print (f"ERROR: Reference mismatch 0x{tag:02x}")
				if tag in ad_types:
					print (f"  \"{ref}\" != \"{ad_types[tag][1]}\"")
				else:
					print (f"  \"{ref}\" != \"{nfc_ad_types[tag][1]}\"")
			if not good or not differ:
				num_errors += 1

###############################################################################
###############################################################################
###############################################################################

def cross_check_psm_values (db):
	global num_errors
	global num_items
	global num_yaml_errors
	global num_xlsx_errors

	rows = db.execute ("SELECT psm, name, ref, pending FROM psms ORDER BY psm")
	psm_values = {}
	for row in rows:
		psm, name, ref, pending = row
		num_items += 1
		if psm in psm_values:
			print (f"ERROR: Duplicate PSM 0x{tag:02x} '{name}' '{psm_values[tag][0]}'")
			num_errors += 1
		else:
			psm_values[psm] = (name, ref, pending)

	data_file = pandas.read_excel ("PSM Values.xlsx", sheet_name = None)

	rows = data_file['Values'].to_dict ('records')

	xl_psm_values = []

	for row in rows:
		psm = row['PSM']
		if isinstance (psm, str):
			psm = full_strip (psm)
			psm = int (psm, 16)
			name = row['Protocol']
			if isinstance (name, str):
				name = full_strip (name)
			else:
				name = ''
			ref = row['Reference']
			if isinstance (ref, str):
				ref = full_strip (ref)
			else:
				ref = ''
			xl_psm_values.append ((psm, name, ref))

	for psm, name, ref in xl_psm_values:
		missing = True # Assume the worst
		good = False # Assume the worst
		differ = False # Assume the worst
		if psm in psm_values:
			missing = False
			if name == psm_values[psm][0]:
				good = True
			if ref in psm_values[psm][1] or psm_values[psm][1] in ref:
				differ = True
		if missing:
			num_errors += 1
			print (f"ERROR: PSM missing 0x{psm:02x} from xlsx")
		else:
			if not good:
				print (f"ERROR: Name mismatch 0x{psm:02x}")
				if psm in psm_values:
					print (f"  \"{name}\" != \"{psm_values[psm][0]}\"")
			if not differ:
				print (f"ERROR: Reference mismatch 0x{psm:02x}")
				if psm in psm_values:
					print (f"  \"{ref}\" != \"{psm_values[psm][1]}\"")
			if not good or not differ:
				num_errors += 1
	
###############################################################################
###############################################################################
###############################################################################

def cross_check_company_ids (db):
	global num_errors
	global num_items
	global num_yaml_errors
	global num_xlsx_errors

	rows = db.execute ("SELECT value, name FROM compid ORDER BY value")
	cid_values = {}
	for row in rows:
		cid, name = row
		num_items += 1
		if cid in cid_values:
			print (f"ERROR: Duplicate CompanyID 0x{value:02x} '{name}' '{cid_values[cid]}'")
			num_errors += 1
		else:
			cid_values[cid] = name

	data_file = pandas.read_excel ("Company IDs.xlsx", sheet_name = None)

	rows = data_file['Company Identifiers'].to_dict ('records')

	xl_cid_values = []

	for row in rows:
		cid = row['Value']
		if isinstance (cid, str):
			cid = full_strip (cid)
			cid = int (cid, 16)
			name = row['Company']
			if isinstance (name, str):
				name = full_strip (name)
			else:
				name = ''
			xl_cid_values.append ((cid, name))

	for cid, name in xl_cid_values:
		missing = True # Assume the worst
		good = False # Assume the worst
		if cid in cid_values:
			missing = False
			if name == cid_values[cid]:
				good = True
		if missing:
			num_errors += 1
			print (f"ERROR: CID missing 0x{cid:02x} from xlsx")
		else:
			if not good:
				print (f"ERROR: Name mismatch 0x{cid:02x}")
				if cid in cid_values:
					print (f"  \"{name}\" != \"{cid_values[cid]}\"")
				num_errors += 1
	
###############################################################################
###############################################################################
###############################################################################

if __name__ == "__main__":
	dot = pathlib.Path ('.')

	path = dot / '..' / '..' / 'SATF-Tools' / 'AssignedNumbers'

	db = sqlite3.connect (path / '_build' / 'assigned_values.db')

	num_errors = 0
	num_items = 0
	num_yaml_errors = 0
	num_xlsx_errors = 0

	# show_all_database_tables (db)
	cross_check_16_bit_uuids (db)
	cross_check_ad_types (db)
	cross_check_psm_values (db)
	cross_check_company_ids (db)

	if num_items > 0:
		print ("")
		print (f"Data Errors {num_errors} / {num_items} {100.0 * num_errors / num_items:0.1f}%")
		print (f"YAML Errors {num_yaml_errors} / {num_items} {100.0 * num_yaml_errors / num_items:0.1f}%")
		print (f"XLSX Errors {num_xlsx_errors} / {num_items} {100.0 * num_xlsx_errors / num_items:0.1f}%")
	else:
		print ("")
		print ("No items considered")

	if num_errors > 0:
		exit (1)
